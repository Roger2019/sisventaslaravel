@extends('layouts.admin')
@section('contenido')
<div class="container"><br>
    <div class="row justify-content-center">
        <div class="card">
        <div class="card-header">
            <strong>Usuarios</strong>
            {{-- <a href="{{url('admin/user/create')}}" class="btn btn-primary float-right">Crear</a> --}}
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-info float-right" data-toggle="modal" data-target="#ModalUserCreate">
            Crear Nuevo Usuario
            </button>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                @include('custom.message')
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Roles(s)</th>
                            <th colspan="4">Acciones</th>
                        </tr>
                    </thead>
                    <body>
                    
                        @foreach($users as $user)
                        <tr>
                            @if ($user->email != 'admin@admin.com')
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                            @isset($user->roles[0]->name)
                                {{$user->roles[0]->name}}
                            @endisset
                            </td>
                            <td>
                            
                                <a href="{{route('user.show',$user->id)}}" class="btn btn-info btn-sm">Mostrar</a>
                            </td>
                            <td>
                                <a href="{{route('user.edit',$user->id)}}" class="btn btn-success btn-sm">Editar</a>
                            </td>
                            <td>
                                <form action="">
                                    @csrf
                                    @method('DELETE')
                                    <button type="button" name="{{$user->id}}" class="btn btn-danger btn-sm btnuserdelete" >Eliminar</button>
                                </form>
                            </td>
                            <td>
                                <button type="button" class="btn btn-secondary btn-sm" onclick="changepassword( {{$user->id}},'{{$user->email}}' );"> Cambiar password</button>
                            </td>
                            @endif
                        </tr>    
                        @endforeach
                   
                    </body>
                </table>
                {{$users->links()}}
            </div>
        </div>
        </div>
        
    </div>
</div>

<!-- Modal crear nuevo usuario para interactuar con el sistema-->
<div class="modal fade" id="ModalUserCreate" tabindex="-1" role="dialog" aria-labelledby="ModalUserCreateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear nuevo usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
            <form id="addnewuser" autocomplete="off">
                <div class="form-group">
                    <input id="name" type="text" class="form-control" name="name" required placeholder="Ingresa el nombre del usuario">
                </div>
                <div class="form-group">
                    <input id="email" type="email" class="form-control" name="email" required placeholder="Ingresa el correo electronico">
                </div>
                <div class="form-group">
                    <input id="npassword" type="password" class="form-control" name="npassword" required placeholder="Ingresa el password"> 
                </div>
                <div class="form-group">
                    <input id="cpassword" type="password" class="form-control" name="cpassword" required placeholder="Confirma el password">
                </div>
            </form>
            <div id="erroruser">

            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
        <button type="button" id="adduser" class="btn btn-secondary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!--Modal para actualizar password-->
<div class="modal fade" id="mostrarmodal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Actualizar el password</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="change_password">
                    <div class="form-group">
                        <input type="text" name="id_user_now" id="id_user_now" hidden="true">
                    </div>
                    <div class="form-group">
                        <input type="text" name="email_user_now" id="email_user_now" class="form-control" disabled>
                    </div>
                    <div class="form-group">
                       <input type="password" name="password" id="password" class="form-control" placeholder="Ingresa el nuevo password">
                    </div>
                </form>
                <div id="errorpass"></div>
            </div>
           <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="closechange" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success" id="btnchangepassword">Actualizar</button>
            </div>
        </div>
    </div>
</div>

@push('scriptusers')
<script src="{{asset('js/funciones_user/user.js')}}"></script>    
@endpush
@endsection