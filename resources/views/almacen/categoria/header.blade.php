<section class="margindivsection">
  <!--https://preview.keenthemes.com/keen/demo1/index.html-->
	<div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center">
      <div>
	      <button type="button" class="btn btn-secondary btn-sm mr-3" id="btnshowmodalcategory">
          <i class="fa fa-archive"></i>
          <strong> Agregar nueva categoria</strong>
        </button>
      </div>
			<!--div>
				<button type="button" class="btn btn-info btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong>Agregar nueva categoria</strong>
        </button>
			</div>
			<div>
				<button type="button" class="btn btn-info btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong>Agregar nueva categoria</strong>
        </button>
			</div-->
		</div>
	</div>
</section>

<!--MODAL PARA AGREGAR LA NUEVA CATEGORIA-->
<div class="modal fade" id="ModalCategoria" tabindex="-1" role="dialog" aria-labelledby="ModalCategoriaLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CategoriaLabel">Agregar nueva categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {!! Form::open(['id'=>'save_categorie'])!!}
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre...">
            </div>
            <div class="form-group">
            	<label for="descripcion">Descripción</label>
            	<input type="text" id="descripcion" name="descripcion" class="form-control" placeholder="Descripción...">
            </div>
             @include('custom.validate_save_form_ajax')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
         <button type="submit" class="btn btn-success" id="btnsavecategoria">Guardar</button>
      </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>

<!--MODAL PARA ACTUALIZAR LA CATEGORIA-->
<div class="modal fade" id="ModalCategoriaUpdate" tabindex="-1" role="dialog" aria-labelledby="ModalCategoriaUpdateLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="CategoriaLabel">Actualizar datos de la categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      {!! Form::open(['id'=>'update_categorie'])!!}
            <input type="text" id="upid" name=upid hidden="true">
            <div class="form-group">
            	<label for="nombre">Nombre</label>
            	<input type="text" id="upnombre" name="upnombre" class="form-control" placeholder="Nombre...">
            </div>
            <div class="form-group">
            	<label for="descripcion">Descripción</label>
            	<input type="text" id="updescripcion" name="updescripcion" class="form-control" placeholder="Descripción...">
            </div>
            @include('custom.validate_update_form_ajax')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-success" id="btnupdatecategoria">Actualizar</button>
      </div>
      {!!Form::close()!!}
    </div>
  </div>
</div>
