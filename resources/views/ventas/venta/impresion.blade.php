<div id="ticket">
    <img
        src="{{asset('../'.$logo)}}"
        alt="Logotipo" id="logo_ticket">
    <p class="centrado" id="sale_cliente"></p>
    <p class="centrado" id="sale_date"></p>
    <table id="content_prod" style="">
        <thead>
            <tr class="tr_table">
                <th class="tick_cant"><small>CANT</small></th>
                <th class="tick_art"><small>ART</small></th>
                <th class="tick_precio"><small>PREC</small></th>
                <th class="tick_desc"><small>DES</small></th>
                <th class="tick_total"><small>TOTAL</small></th>
            </tr>
        </thead>
        <tbody id="tbody_details">
        </tbody>
        <tfoot>
             <tr class="tr_table">
                <td class="" colspan="4"><small>VENTA TOTAL</small></td>
                <td class="" id="sale_total"></td>
            </tr>
            <tr class="tr_table">
                <td class="" colspan="4"><small>CAMBIO</small></td>
                <td class="" id="sale_cambio"></td>
            </tr>
        </tfoot>
    </table>
    <div class="centrado">
        <p><small> FOLIO :</small> <span id="sale_folio"></span></p>
    </div>
    <p class="centrado"><small>¡GRACIAS POR SU COMPRA!<br>{{$name}}</small></p>
</div>


<!-- Modal success and ticket-->
<div class="modal fade" id="printModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-content-cambio">
    <div class="modal-content">
      <div class="modal-header bg-secondary">
        <h5 class="modal-title text-center" id="messsageModalLabelSuccess"></h5>
        <!--button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button-->
      </div>
      <div class="modal-body">
        <br>
        <form id="send-email-now">
          @csrf
          <input type="number" id="cons_send_email" name="cons_send_email" hidden="true">
          <input type="number" id="suelt_vent" name="suelt_vent" hidden="true">
          <div class=" text-center">
            <h5><strong> EL CAMBIO ES DE $ : <span id="cambio_sale"></span></strong></h5>
          </div>
          <div id="show_input_email" style="display:none;">
            <div class="row g-3" >
              <div class="col-sm-9">
                <input type="text" class="form-control" name="nowemail" id="nowemail" placeholder="Ingresa el correo electronico" >
              </div>
              <div class="col-sm">
                <button class="btn btn-info btn-block btn-flat" id="btn-send-ticket-email">Aceptar</button>
              </div>
            </div>
          </div>
        </form>
        <div class="">
            <div class="alert alert-danger show_alert_modal" style="display:none;" role="alert"></div>
        </div>
      </div>
      <div class="modal-footer">
        <!--button type="button" class="btn btn-secondary btn-flat" data-bs-dismiss="modal" id="btn-close-modal"><strong>Cerrar la ventana</strong></button>
        <button type="button" class="btn btn-success btn-flat " id="btn-show-email"><strong>Enviar al correo</strong> </button>
        <button type="button" class="btn btn-primary btn-flat" id="print_now_ticket"><strong>Imprimir el ticket</strong></button-->
        <div class="container text-center">
        <button type="button" class="btn btn-default btn4" data-bs-dismiss="modal" id="btn-close-modal"><i class="fas fa-times-circle mr-1"></i> Cerrar ventana</button>
        <button type="button" class="btn btn-default btn3" id="btn-show-email"><i class="fas fa-envelope-open-text mr-2"></i> Enviar correo</button>
        <button type="button" class="btn btn-default btn2" id="print_now_ticket"><i class="fas fa-print mr-2"></i> Imprimir ticket</button>
        </div>
      </div>
    </div>
  </div>
</div>

<template id="template_details">
    <tr class="tr_table">
        <td class="tick_cant detail_cantidd"></td>
        <td class="tick_art detail_nombre"></td>
        <td class="tick_precio detail_venta"></td>
        <td class="tick_desc detail_descuento"></td>
        <td class="tick_total detail_subtotal"></td>
    </tr>
</template>

<!--div class="" tabindex="">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-secondary">
        <h5 class="modal-title">Modal title</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class=" text-center">
          <h5><strong> EL CAMBIO ES DE $ : <span id="cambio_sale"></span></strong></h5>
        </div>

        <div id="show_input_email" style="display:none;">

        <div class="row g-3" >
          <div class="col-sm-9">
            <input type="text" class="form-control" placeholder="Ingresa el correo electronico" >
          </div>
          <div class="col-sm">
            <button class="btn btn-info btn-block btn-flat">Aceptar</button>
          </div>
        </div>

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-flat" data-bs-dismiss="modal"><strong>Cerrar la ventana</strong></button>
        <button type="button" class="btn btn-success btn-flat " id="btn-show-email"><strong>Enviar al correo</strong> </button>
        <button type="button" class="btn btn-primary btn-flat" id="print_now_ticket"><strong>Imprimir el ticket</strong></button>
      </div>
    </div>
  </div>
</div-->
