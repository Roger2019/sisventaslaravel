<section class="content margindivsection">
	<div class="d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
		<div class="d-flex align-items-center">
      <div>
	      <!--a href="{{route('venta.create')}}" class="btn btn-secondary btn-sm mr-3">
          <i class="fa fa-archive"></i>
          <strong> Realizar una venta</strong>
        </a-->
        <button type="button" class="btn btn-secondary btn-sm mr-3" data-toggle="modal" data-target="#ModalSaveCliente">
        <i class="fa fa-archive"></i>
         Registrar nuevo cliente
        </button>
      </div>
		</div>
	</div>
</section>

<!-- MODAL PARA AGREGAR NUEVO CLIENTE-->
<div class="modal fade" id="ModalSaveCliente" tabindex="-1" role="dialog" aria-labelledby="ModalSaveClienteLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalSaveClienteLabel">Agregar nuevo cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <form id="save_cliente" autocomplete="off">
            @csrf
            <div class="form-group">
            <label>Nombre</label>
                <input type="text" class="form-control" name="nombre">
            </div>
             <div class="form-group">
            <label>Direccion</label>
                <input type="text" class="form-control" name="direccion">
            </div>
             <div class="form-group">
            <label>Telefono</label>
                <input type="number" class="form-control" name="telefono">
            </div>
            <div class="form-group">
            <label>Email</label>
                <input type="text" class="form-control" name="email">
            </div>
             @include('custom.validate_save_form_ajax')
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="btn_hide_save_modal" data-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" id="btnsavecliente">Guardar</button>
            </form>
      </div>
    </div>
  </div>
</div>

<!-- MODAL PARA ACTUALIZAR NUEVO CLIENTE-->
<div class="modal fade" id="ModalUpdateCliente" tabindex="-1" role="dialog" aria-labelledby="ModalSaveClienteLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="">Actualizar cliente</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="update_cliente" autocomplete="off">
          @csrf
          <input type="text" id="clienteupdate" name="clienteupdate" hidden="true">
          <div class="form-group">
          <label>Nombre</label>
              <input type="text" class="form-control" name="updnombre" id="updnombre">
          </div>
            <div class="form-group">
          <label>Direccion</label>
              <input type="text" class="form-control" name="upddireccion" id="upddireccion">
          </div>
            <div class="form-group">
          <label>Telefono</label>
              <input type="number" class="form-control" name="updtelefono" id="updtelefono">
          </div>
          <div class="form-group">
          <label>Email</label>
              <input type="text" class="form-control" name="updemail" id="updemail">
          </div>
          @include('custom.validate_update_form_ajax')
        </form>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-secondary" id="hide_update_modal" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" class="btn btn-primary" id="btnupdatecliente">Actualizar cliente</button>
      </div>
    </div>
  </div>
</div>